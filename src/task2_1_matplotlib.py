import matplotlib.pyplot as plt
# noinspection PyUnresolvedReferences
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


def z1(x, y):
    """
    (x^2 + y^2) / 5 - 2cos(x/2) - 6cos(y) - 8
    """
    return (x ** 2 + y ** 2) / 5 - 2 * np.cos(x / 2) - 6 * np.cos(y) - 8


def z2(x, y):
    """
    (x/2)^5 + (y/2)^4 - 4
    """
    return (x / 2) ** 5 + (y / 2) ** 4 - 4


fig = plt.figure()
ax: Axes3D = fig.gca(projection="3d")


def plot_surface(ax: Axes3D, x_from, x_to, y_from, y_to, z_func, step, z_from=None, z_to=None):
    x = np.arange(x_from, x_to, step, dtype=float)
    y = np.arange(y_from, y_to, step, dtype=float)
    x, y = np.meshgrid(x, y, sparse=True)
    z = z_func(x, y)

    z[np.logical_or(z < z_from, z > z_to)] = np.nan

    return ax.plot_surface(x, y, z)


plot_size = 10
plot_step = 1

# plot_surface(ax, -plot_size, plot_size, -plot_size, plot_size, z1, plot_step)
plot_surface(ax, -plot_size, plot_size, -plot_size, plot_size, z2, plot_step, -plot_size * 2, plot_size * 2)

ax.auto_scale_xyz(*[[-plot_size, plot_size]] * 3, had_data=False)
plt.show()
