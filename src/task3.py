import math
import random

import matplotlib.pyplot as plt
import numpy as np


def target(points, s):
    lengths = np.array(
        [
            math.sqrt(sum(np.square(points[i] - points[j])))
            for i in range(len(points) - 1) for j in range(i + 1, len(points))
        ]
    ).flatten()
    average_length = np.average(lengths)
    average_diff = sum(np.abs(lengths - average_length))

    s_diff = abs(sum(lengths) - s)

    return average_diff + s_diff


def qvazi_gradient(points, delta, target, *args, **kwargs):
    current_target = target(points, *args, **kwargs)
    gradient = []

    for i, point in enumerate(points):
        point_gradient = []

        for j, coord in enumerate(point):
            if i == 0:  # the first point [0, 0] is fixed in place
                point_gradient.append(0)
            else:
                modified_points = points.copy()
                modified_points[i][j] += delta

                point_gradient.append((target(modified_points, *args, **kwargs) - current_target) / delta)

        point_gradient = np.array(point_gradient)

        if point_gradient.any():
            point_gradient /= np.linalg.norm(point_gradient)

        gradient.append(point_gradient)

    return np.array(gradient)


def broiden_gradient(points, prev_points, prev_gradient, target, prev_target):
    s = (points - prev_points).flatten("F")[None].T
    y = target - prev_target

    prev_gradient = prev_gradient.flatten("F")

    gradient = prev_gradient + np.matmul(y - np.matmul(prev_gradient, s), s.T) / np.matmul(s.T, s)

    gradient = np.reshape(gradient, points.shape, "F")

    for point in gradient:
        if point.any():
            point /= np.linalg.norm(point)

    return gradient


num_points = 4
x_min, x_max, y_min, y_max = -10, 10, -10, 10
s_target = 10
step = 1
qvazi_gradient_delta = 0.001
iterations = 100
accuracy = 1e-3

random.seed(100)

points = np.array(
    [[0.0, 0.0]] +
    [[random.uniform(x_min, x_max), random.uniform(y_min, y_max)] for i in range(num_points - 1)]
)

gradient = qvazi_gradient(points, qvazi_gradient_delta, target, s_target)
current_target = target(points, s_target)

print(points, "points")
print(gradient, "initial gradient")

for i in range(iterations):
    plt.clf()
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    plt.scatter(points[:, 0], points[:, 1])
    plt.title(f"Target: {current_target} | Iteration: {i + 1}")

    plt.pause(0.001)
    # plt.waitforbuttonpress()

    prev_points = points
    points = prev_points - step * gradient
    prev_target = current_target
    current_target = target(points, s_target)

    if current_target <= accuracy:
        break

    if prev_target < current_target:
        gradient = qvazi_gradient(points, qvazi_gradient_delta, target, s_target)
        step *= 0.95
    else:
        gradient = broiden_gradient(points, prev_points, gradient, current_target, prev_target)


plt.show()
